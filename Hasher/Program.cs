﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Hasher
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Drag and drop a folder onto Hasher's icon.");

                return;
            }

            if (!Directory.Exists(args[0]))
            {
                Console.WriteLine("ERROR: The directory specified does not exist.");

                return;
            }

            Console.WriteLine("Hashing in progress, please wait...");

            string rootDirFile = new DirectoryInfo(args[0]).Name + "_HASH.txt";

            if (File.Exists(rootDirFile))
                File.Delete(rootDirFile);

            using (var hasher = new SHA1Cng())
            {
                byte[] buffer = new byte[1024 * 1024 * 50];

                int bytesRead = 0;

                foreach (var fileFull in Directory.EnumerateFiles(args[0], "*.*", SearchOption.AllDirectories))
                {
                    hasher.Initialize();

                    string fileShort = fileFull.Remove(0, args[0].Length).TrimStart(new char[] { '\\' });

                    using (var fs = File.OpenRead(fileFull))
                    {
                        int currentLineCursor = Console.CursorTop;

                        while ((bytesRead = fs.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            Console.SetCursorPosition(0, currentLineCursor);

                            Console.Write(new string(' ', Console.WindowWidth));

                            Console.SetCursorPosition(0, currentLineCursor);

                            Console.Write(fileShort + ": " + ((double)(fs.Position * 100) / fs.Length).ToString("0.00") + "%");

                            hasher.TransformBlock(buffer, 0, bytesRead, null, 0);
                        }
                    }

                    hasher.TransformFinalBlock(new byte[0], 0, 0);

                    string hash = BitConverter.ToString(hasher.Hash).Replace("-", "").ToUpper();

                    File.AppendAllText(rootDirFile, fileShort + " " + hash + Environment.NewLine, Encoding.UTF8);
                }
            }
        }
    }
}
